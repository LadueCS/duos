# DUOS

DUOS is a powerful and flexible Arch-based Linux distribution, made by LadueCS.

![DUOS](https://raw.githubusercontent.com/LadueCS/duos/main/screenshot.png)


## Installation

Download the latest live image from the [releases](https://github.com/LadueCS/duos/releases), then boot it and log in as the duos user with password duos.

You can either install DUOS using the [Arch installation guide](https://wiki.archlinux.org/title/Installation_guide) or use a script like [archinstall](https://wiki.archlinux.org/title/Archinstall). At the end of the installation, make sure you run `cp /etc/pacman.conf /mnt/etc/pacman.conf` to copy over the custom DUOS configuration.


You can also convert an existing Arch Linux installation to DUOS by adding the following files to the **top** of the Repositories section in `/etc/pacman.conf`:

```
[duos]
Server = https://laduecs.github.io/duos/pkg/
SigLevel = PackageOptional
```

You may have to run `sudo pacman -S duos/filesystem` to install the DUOS version of the filesystem package.

